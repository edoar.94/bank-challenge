defmodule BankChallenge.Core.Schemas.Account do
  use Ecto.Schema
  import Ecto.Changeset

  alias BankChallenge.Core.Schemas.Card

  @derive {Jason.Encoder, except: [:__meta__, :cards, :inserted_at, :updated_at]}
  schema "accounts" do
    field :name, :string, default: "Default"
    field :lastname, :string, default: "account"
    field :phone, :string, default: "0000000000"
    field :email, :string, default: "email@example.com"
    field :account_number, :string, default: "0000000000"

    has_many :cards, Card

    timestamps()
  end

  @required_fields ~w(name lastname email account_number)a
  @optional_fields ~w(phone)a
  def changeset(%__MODULE__{} = data, params \\ %{}) do
    cast(data, params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:phone, is: 10)
    |> validate_format(:phone, ~r{\A\d*\z})
  end
end
