defmodule BankChallengeWeb.AccountView do
  use BankChallengeWeb, :view

  alias BankChallengeWeb.CardView

  def render("index.json", %{accounts: accounts}) do
    %{
      data: render_many(accounts, __MODULE__, "account.json")
    }
  end

  def render("show.json", %{account: account}) do
    %{data: render_one(account, __MODULE__, "account.json")}
  end

  def render("account.json", %{account: account}) do
    account(account)
  end

  def tab_active(current, tab) do
    if String.to_integer(current) == tab do
      "active show"
    end
  end

  defp account(account) do
    %{
      id: account.id,
      name: account.name,
      lastname: account.lastname,
      phone: account.phone,
      email: account.email,
      account_number: account.account_number,
    }
  end
end
