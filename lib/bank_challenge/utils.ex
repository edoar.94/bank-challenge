defmodule BankChallenge.Utils do
  def to_format_api_resp(data) do
    Jason.encode!(data)
    |> Jason.decode!()
  end

  def transform_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, &format_error /1)
    |> Enum.map(fn {key, msg} ->
      %{key: key, message: List.first(msg)}
    end)
  end

  defp format_error ({msg, opts}) do
    Enum.reduce(opts, msg, fn {key, value}, acc ->
      String.replace(acc, "%{#{key}}", to_string(value))
    end)
  end
end
