# BankChallenge

project requirements

  * Elixir 1.11.0
  * Erlang 23.3
  * Nodejs 14.15.0

To start your Phoenix server:

  * Edit `.env_example` with valid credentials for database
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

To add new transactions you can use:
  * Method: `POST`
  * URL: `http://localhost:4000/api/ACCOUNT_ID/cards/CARD_ID/transactions`
  * ACCOUNT_ID: 1
  * CARD_ID: `1` for credit and `2` for debit
  * Params:

```
{
  "transaction": {
    "title": "Test",
    "description": "test transaction",
    "date": "2021-11-17 02:20:07",
    "type": "deposit" | "withdrawal",
    "status": "complete" | "pending",
    "amount": 100
  }
}
```
note: account and card id's are default data when you run `mix ecto.setup` or `mix run priv/repo/seed.exs`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
