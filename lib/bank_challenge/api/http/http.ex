defmodule BankChallenge.APIRequest do
  @moduledoc """
  Wraps request to local API
  """
  require HTTPoison

  @doc """
  Fetch data with local API url

  ## Examples:

  iex> BankChallenge.APIRequest.get(url)
  dinamyc_response
  """
  def get(url) do
    HTTPoison.get(base_url() <> url)
    |> handle_response()
  end

  @doc false
  defp handle_response(response) do
    case response do
      {:ok, %HTTPoison.Response{status_code: status_code} = response} when status_code in 200..299 ->
        {:ok, Jason.decode!(response.body)}

      {:ok, %HTTPoison.Response{status_code: status_code} = response} when status_code in 400..599 ->
        {:error, Jason.decode!(response.body)}

      {:error, %HTTPoison.Error{reason: reason}} ->
      {:error, %{reason: reason}}
    end
  end

  @doc false
  defp base_url,
    do: "http://localhost:4000/api/"
end
