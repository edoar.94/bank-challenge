defmodule BankChallenge.Core.Methods.AccountMethods do
  @moduledoc """
  Account methods.
  """
  alias BankChallenge.{Core.Schemas.Account, Repo}

  @doc """
  Fetch all accounts in database.

  ## Examples
  iex> BankChallenge.Core.Methods.AccountMethods.accounts()
  [%BankChallenge.Core.Schemas.Account{...}, ...]
  """
  def accounts,
    do: Repo.all(Account)

  @doc """
  Fetch an account by id, returns `nil` if account doesn't exist.

  ## Examples
  iex> BankChallenge.Core.Methods.AccountMethods.fetch_account(id)
  %BankChallenge.Core.Schemas.Account{...}

  iex> BankChallenge.Core.Methods.AccountMethods.fetch_account(wrong_id)
  nil
  """
  def fetch_account(id),
    do: Repo.get(Account, id)

  @doc """
  Create an account, returns %Ecto.Changeset{} if fails.

  ## Examples
  iex> BankChallenge.Core.Methods.AccountMethods.create_account(params)
  {:ok, %BankChallenge.Core.Schemas.Account{...}}

  iex> BankChallenge.Core.Methods.AccountMethods.create_account(params)
  {:error, %Ecto.Changeset{...}}
  """
  def create_account(attrs \\ %{}) do
    Account.changeset(%Account{}, attrs)
    |> Repo.insert()
  end

  @doc """
  Update and account, returns %Ecto.Changeset{} if fails.

  ## Examples
  iex> BankChallenge.Core.Methods.AccountMethods.update_account(params)
  {:ok, %BankChallenge.Core.Schemas.Account{...}}

  iex> BankChallenge.Core.Methods.AccountMethods.update_account(params)
  {:error, %Ecto.Changeset{...}}
  """
  def update_account(attrs) do
    fetch_account(attrs.id)
    |> Account.changeset(attrs)
    |> Repo.update()
  end
end
