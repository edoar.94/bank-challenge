defmodule BankChallenge.Repo do
  use Ecto.Repo,
    otp_app: :bank_challenge,
    adapter: Ecto.Adapters.Postgres
end
