defmodule BankChallenge.Core.Methods.CardMethodsTest do
  use BankChallengeWeb.ConnCase
  alias BankChallenge.Core.{Methods.CardMethods, Methods.TransactionMethods, Schemas.Card}

  setup do
    %{
      cards: CardMethods.cards(1),
      card: CardMethods.fetch_card(1),
      tr_params: %{title: "Transaction", descriprion: "update card balance transaction", status: "complete", type: "withdrawal", card_id: 1}
    }
  end

  describe "Card core methods: cards/1" do
    test "Fetch all cards by account id", %{cards: cards} do
      assert CardMethods.cards(1) == cards
    end
  end

  describe "Card core methods: fetch_card/1" do
    test "Fetch card by id", %{card: card} do
      assert CardMethods.fetch_card(1) == card
    end
  end

  describe "Card core methods: create_card/1" do
    test "Create card" do
      params = %{account_id: 1, type: "debit", card_number: "9999888877776666", balance: 100}
      assert {:ok, card} = CardMethods.create_card(params)
    end
  end

  describe "Card core methods: update_card/1" do
    test "Update card" do
      params = %{id: 1, account_id: 1, type: "credit", balance: 2_500}
      assert {:ok, card} = CardMethods.update_card(params)
    end
  end

  describe "Card core methods: calculate_balance/1" do
    test "Calculate card balance and return it into changeset", %{tr_params: tr_params} do
      assert {:ok, transaction} = TransactionMethods.create_transaction(tr_params)
      assert %Ecto.Changeset{} = CardMethods.calculate_balance(transaction)
    end
  end
end
