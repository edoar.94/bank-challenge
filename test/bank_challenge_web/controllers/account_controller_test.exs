defmodule BankChallengeWeb.AccountControllerTest do
  use BankChallengeWeb.ConnCase

  alias BankChallenge.{Core.Methods.AccountMethods, Utils}

  setup do
    %{
      accounts: AccountMethods.accounts() |> Utils.to_format_api_resp(),
      account: AccountMethods.fetch_account(1) |> Utils.to_format_api_resp()
    }
  end

  describe "Accounts API: index/2" do
    test "Fetch all accounts on database", %{conn: conn, accounts: accounts} do
      conn = get(conn, "/api/accounts")

      assert json_response(conn, 200)["data"] == accounts
    end
  end

  describe "Accounts API: show/2" do
    test "Fetch account by id from database", %{conn: conn, account: account} do
      conn = get(conn, "/api/accounts/1")

      assert json_response(conn, 200)["data"] == account
    end
  end
end
