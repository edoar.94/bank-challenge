defmodule BankChallengeWeb.TransactionControllerTest do
  use BankChallengeWeb.ConnCase

  alias BankChallenge.Core.Methods.TransactionMethods

  setup do
    %{
      account_id: 1,
      card_id: 1
    }
  end

  describe "Transactions API: index/1" do
    test "Fetch all transactions by card id", %{conn: conn, account_id: account_id, card_id: card_id} do
      conn = get(conn, "/api/accounts/#{account_id}/cards/#{card_id}/transactions")

      assert %{"data" => transactions} = json_response(conn, 200)
    end
  end

  describe "Transactions API: create_transaction/2" do
    test "Create transaction from API", %{conn: conn, account_id: account_id, card_id: card_id} do
      params = %{title: "API Test", description: "test from API request", amount: 50, type: "deposit", status: "complete", card_id: card_id}
      conn = post(conn, "/api/accounts/#{account_id}/cards/#{card_id}/transactions", %{transaction: params})

      assert transaction = json_response(conn, 200)
    end
  end
end
