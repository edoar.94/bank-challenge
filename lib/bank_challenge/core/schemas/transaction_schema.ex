defmodule BankChallenge.Core.Schemas.Transaction do
  use Ecto.Schema
  import Ecto.Changeset

  alias BankChallenge.Core.Schemas.Card

  schema "transactions" do
    field :title, :string, default: "transaction"
    field :description, :string, default: "basic description"
    field :date, :utc_datetime, default: DateTime.now!("Etc/UTC") |> DateTime.truncate(:second)
    field :amount, :integer, default: 0
    field :type, :string, default: "withdrawal"
    field :status, :string, default: "pending"

    belongs_to :card, Card

    timestamps()
  end

  @required_fields ~w(amount card_id date status title type)a
  @optional_fields ~w(description)a
  def changeset(%__MODULE__{} = data, params \\ %{}) do
    cast(data, params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end
end
