defmodule BankChallenge.Repo.Migrations.Transaction do
  use Ecto.Migration

  def up do
    create table(:transactions) do
      add :date, :utc_datetime
      add :amount, :bigint
      add :title, :string
      add :description, :string
      add :status, :string
      add :type, :string

      add :card_id, references(:cards)

      timestamps()
    end
  end

  def down do
    drop table(:transactions)
  end
end

# types
# - withdraw
#
#
#