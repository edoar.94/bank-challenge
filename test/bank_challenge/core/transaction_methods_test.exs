defmodule BankChallenge.Core.Methods.TransactionMethodsTest do
  use BankChallengeWeb.ConnCase
  alias BankChallenge.Core.{Methods.TransactionMethods, Schemas.Transaction}

  setup do
    %{
      transactions: TransactionMethods.transactions(1),
      params: %{title: "Test", descriprion: "first transaction", status: "complete", type: "withdrawal", card_id: 1}
    }
  end

  describe "Transaction core methods: transactions/1" do
    test "Fetch transactions by card id", %{transactions: transactions} do
      assert TransactionMethods.transactions(1) == transactions
    end
  end

  describe "Transaction core methods: fetch_transaction/1" do
    test "Fetch transaction by id", %{params: params} do
      {:ok, %{id: id} = transaction} = TransactionMethods.create_transaction(params)
      assert TransactionMethods.fetch_transaction(id) == transaction
    end
  end

  describe "Transaction core methods: create_transaction/1" do
    test "Create transaction", %{params: params} do
      assert {:ok, transaction} = TransactionMethods.create_transaction(params)
    end
  end

  describe "Transaction core methods: update_transaction/1" do
    test "Update transaction", %{params: params} do
      {:ok, %{id: id} = transaction} = TransactionMethods.create_transaction(params)
      upd_attrs = %{id: id, title: "Test Updated", description: "Updated transaction"}

      assert {:ok, updated_transaction} = TransactionMethods.update_transaction(upd_attrs)
    end
  end

  describe "Transaction core methods: transaction_multi/1" do
    test "Create a transaction and update card balance with Ecto.Multi", %{params: params} do
      assert {:ok, %{card: card, transaction: transaction}} = TransactionMethods.transaction_multi(params)
    end
  end
end
