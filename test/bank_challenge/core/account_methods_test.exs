defmodule BankChallenge.Core.Methods.AccountMethodsTest do
  use BankChallengeWeb.ConnCase

  alias BankChallenge.Core.{Methods.AccountMethods, Schemas.Account}

  setup do
    %{
      accounts: AccountMethods.accounts(),
      account: AccountMethods.fetch_account(1)
    }
  end

  describe "accounts core methods: accounts/0" do
    test "Fetch all accounts from database", %{accounts: accounts} do
      assert AccountMethods.accounts() == accounts
    end
  end

  describe "accounts core methods: fetch_account/1" do
    test "Fetch account by id from database", %{account: account} do
      assert AccountMethods.fetch_account(1) == account
    end
  end

  describe "accounts core methods: create_account/1" do
    test "Create account" do
      params = %{name: "test", lastname: "account", phone: "0123456789", email: "test@email.com", account_number: "012345"}
      assert {:ok, account_created} = AccountMethods.create_account(params)
    end
  end

  describe "accounts core methods: update_account/1" do
    test "Update account" do
      params = %{id: 1, phone: "6584759874", email: "update_test@email.com", account_number: "012345"}
      assert {:ok, account_updated} = AccountMethods.update_account(params)
    end
  end
end
