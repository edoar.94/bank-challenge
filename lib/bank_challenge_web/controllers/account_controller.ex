defmodule BankChallengeWeb.AccountController do
  use BankChallengeWeb, :controller

  alias BankChallenge.Core.Methods.AccountMethods

  def index(conn, _params) do
    accounts = AccountMethods.accounts()

    render(conn, "index.json", accounts: accounts)
  end

  def show(conn, %{"id" => id}) do
    account = AccountMethods.fetch_account(id)

    render(conn, "show.json", account: account)
  end
end
