defmodule BankChallengeWeb.Router do
  use BankChallengeWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :live_view do
    plug :put_root_layout, {BankChallengeWeb.LayoutView, :root}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BankChallengeWeb do
    pipe_through [:browser, :live_view]

    # get "/", PageController, :index
    live "/", BankLive
  end

  scope "/api", BankChallengeWeb do
    pipe_through :api

    resources "/accounts", AccountController do
      resources "/cards", CardController do
        resources "/transactions", TransactionController
      end
    end
  end
end
