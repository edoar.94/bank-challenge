defmodule BankChallenge.Core.Methods.CardMethods do
  @moduledoc """
  Card methods.
  """
  import Ecto.Query
  alias BankChallenge.{Core.Schemas.Card, Repo}

  @doc """
  Fetch all cards in database by account id.

  ## Examples

  iex> BankChallenge.Core.Methods.CardMethods.cards(account_id)
  [%BankChallenge.Core.Schemas.Card{...}, ...]
  """
  def cards(account_id) do
    query = from c in Card, where: c.account_id == ^account_id

    Repo.all(query)
  end

  @doc """
  Fetch a card by id, return `nil` if card doesn't exist.

  ## Examples

  iex> BankChallenge.Core.Methods.CardMethods.fetch_card(card_id)
  %BankChallenge.Core.Schemas.Card{...}

  iex> BankChallenge.Core.Methods.CardMethods.fetch_card(wrong_id)
  nil
  """
  def fetch_card(id),
    do: Repo.get(Card, id)

  @doc """
  Create a card, returns %Ecto.Changeset{} if fails.

  ## Examples
  iex> BankChallenge.Core.Methods.CardMethods.create_card(params)
  {:ok, %BankChallenge.Core.Schemas.Card{...}}

  iex> BankChallenge.Core.Methods.CardMethods.create_card(params)
  {:error, %Ecto.Changeset{...}}
  """
  def create_card(attrs \\ %{}) do
    Card.changeset(%Card{}, attrs)
    |> Repo.insert()
  end

  @doc """
  Create a card, returns %Ecto.Changeset{} if fails.

  ## Examples
  iex> BankChallenge.Core.Methods.CardMethods.update_card(params)
  {:ok, %BankChallenge.Core.Schemas.Card{...}}

  iex> BankChallenge.Core.Methods.CardMethods.update_card(params)
  {:error, %Ecto.Changeset{...}}
  """
  def update_card(attrs) do
    fetch_card(attrs.id)
    |> Card.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Calculate new card balance from transaction record

  ## Examples
  iex> BankChallenge.Core.Methods.CardMethods.calculate_balance(params)
  %BankChallenge.Core.Schemas.Card{...}
  """
  def calculate_balance(transaction) do
    card = fetch_card(transaction.card_id)
    new_amount =
      case card.type do
        "debit" ->
          update_debit_card(card.balance, transaction)
        "credit" ->
          update_credit_card(card.balance, transaction)
      end

    Card.changeset(card, %{balance: new_amount})
  end

  @doc false
  defp update_debit_card(balance, transaction) do
    case transaction.type do
      "withdrawal" ->
        balance - transaction.amount
      "deposit" ->
        balance + transaction.amount
    end
  end

  @doc false
  defp update_credit_card(balance, transaction) do
    case transaction.type do
      "withdrawal" ->
        balance + transaction.amount
      "deposit" ->
        balance - transaction.amount
    end
  end
end
