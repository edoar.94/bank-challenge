defmodule BankChallenge.Core.Methods.TransactionMethods do
  @moduledoc """
  Transaction methods.
  """
  import Ecto.Query
  alias BankChallenge.{Core.Methods.CardMethods, Core.Schemas.Transaction, Repo}
  alias Ecto.Multi

  @doc """
  Fetch all transaction by card_id.

  ## Examples:

  iex> BankChallenge.Core.Methods.TransactionMethods.transactions(card_id)
  [%BankChallenge.Core.Schemas.Transaction{...}, ...]
  """
  def transactions(card_id) do
    query = from t in Transaction, where: t.card_id == ^card_id

    Repo.all(query)
  end

  @doc """
  Fetch a transaction by id, return nil if record not found

  ## Examples:

  iex> BankChallenge.Core.Methods.TransactionMethods.fetch_transaction(transaction_id)
  %BankChallenge.Core.Schemas.Transaction{}

  iex> BankChallenge.Core.Methods.TransactionMethods.fetch_transaction(wrong_id)
  nil
  """
  def fetch_transaction(id),
    do: Repo.get(Transaction, id)

  @doc """
  Create a transaction, returns %Ecto.Changeset{} if fails.

  ## Examples
  iex> BankChallenge.Core.Methods.TransactionMethods.create_transaction(params)
  {:ok, %BankChallenge.Core.Schemas.Transaction{...}}

  iex> BankChallenge.Core.Methods.TransactionMethods.create_transaction(params)
  {:error, %Ecto.Changeset{...}}
  """
  def create_transaction(attrs \\ %{}) do
    Transaction.changeset(%Transaction{}, attrs)
    |> Repo.insert()
  end

  @doc """
  Update a transaction, returns %Ecto.Changeset{} if fails.

  ## Examples
  iex> BankChallenge.Core.Methods.TransactionMethods.update_transaction(params)
  {:ok, %BankChallenge.Core.Schemas.Transaction{...}}

  iex> BankChallenge.Core.Methods.TransactionMethods.update_transaction(params)
  {:error, %Ecto.Changeset{...}}
  """
  def update_transaction(attrs) do
    fetch_transaction(attrs.id)
    |> Transaction.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Crate a transaction and update card balance, return error tuples if fails.

  ## Examples:

  iex> BankChallenge.Core.Methods.TransactionMethods.transaction_multi(params)
  {:ok, %{card: %BankChallenge.Core.Schemas.Card{}, transaction: %BankChallenge.Core.Schemas.Transaction{}}}

  iex> BankChallenge.Core.Methods.TransactionMethods.transaction_multi(wrong_params)
  {:error, operation, error_changeset, changes}
  """
  def transaction_multi(attrs \\ %{}) do
    Multi.new()
    |> Multi.insert(:transaction, Transaction.changeset(%Transaction{}, attrs))
    |> Multi.update(:card, fn %{transaction: transaction} ->
      CardMethods.calculate_balance(transaction)
    end)
    |> Repo.transaction()
  end
end
