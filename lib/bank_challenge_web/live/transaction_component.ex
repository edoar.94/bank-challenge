defmodule BankChallengeWeb.TransactionComponent do
  use Phoenix.LiveComponent
  alias BankChallenge.APIRequest
  alias BankChallengeWeb.AccountView

  def render(assigns),
    do: Phoenix.View.render(AccountView, "transaction_component.html", assigns)

  def update(assigns, socket) do
    socket = assign(socket, %{
      tab: assigns.tab,
      card_id: assigns.id,
      account_id: assigns.account_id,
      transactions: request_transaction(assigns.account_id, assigns.id)
    })

    {:ok, socket}
  end

  defp request_transaction(account_id, card_id) do
    {:ok, %{"data" => transaction}} = APIRequest.get("accounts/#{account_id}/cards/#{card_id}/transactions")
    transaction
  end
end
