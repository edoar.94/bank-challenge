defmodule BankChallenge.Repo.Migrations.Cards do
  use Ecto.Migration

  def up do
    create table(:cards) do
      add :type, :string
      add :card_number, :string
      add :balance, :bigint

      add :account_id, references(:accounts)

      timestamps()
    end
  end

  def down do
    drop table(:cards)
  end
end
