# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BankChallenge.Repo.insert!(%BankChallenge.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BankChallenge.Core.Schemas.{Account, Card, Transaction}
alias BankChallenge.Repo

# Insert default account
Repo.insert! %Account{
  name: "Basic",
  lastname: "Account"
}

# Insert cards
Repo.insert! %Card{
  account_id: 1,
  type: "credit",
  card_number: "1234123412341234",
  balance: 5_000
}

Repo.insert! %Card{
  account_id: 1,
  type: "debit",
  card_number: "1111222233334444"
}