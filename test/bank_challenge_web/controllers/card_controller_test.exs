defmodule BankChallengeWeb.CardControllerTest do
  use BankChallengeWeb.ConnCase

  alias BankChallenge.{Core.Methods.CardMethods, Utils}

  setup do
    %{
      cards: CardMethods.cards(1) |> Utils.to_format_api_resp()
    }
  end

  describe "Card API: index/1" do
    test "Fetch all cards by account id", %{conn: conn, cards: cards} do
      conn = get(conn, "/api/accounts/1/cards")

      assert json_response(conn, 200)["data"] == cards
    end
  end
end
