defmodule BankChallengeWeb.TransactionView do
  use BankChallengeWeb, :view

  def render("index.json", %{transactions: transactions}) do
    %{
      data: render_many(transactions, __MODULE__, "transaction.json")
    }
  end

  def render("transaction.json", %{transaction: transaction}) do
    transaction(transaction)
  end

  defp transaction(transaction) do
    %{
      id: transaction.id,
      title: transaction.title,
      description: transaction.description,
      date: transaction.date,
      amount: transaction.amount,
      type: transaction.type,
      status: transaction.status
    }
  end
end
