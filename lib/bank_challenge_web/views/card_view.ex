defmodule BankChallengeWeb.CardView do
  use BankChallengeWeb, :view

  # alias BankChallengeWeb.TransactionView

  def render("index.json", %{cards: cards}) do
    %{
      data: render_many(cards, __MODULE__, "card.json")
    }
  end

  def render("card.json", %{card: card}) do
    card(card)
  end

  def render("card_transactions.json", %{card: card}) do
    card(card)
    |> Map.merge(
      %{
        # transactions: render_many()
      }
    )
  end

  defp card(card) do
    %{
      id: card.id,
      account_id: card.account_id,
      type: card.type,
      card_number: card.card_number,
      balance: card.balance,
    }
  end
end
