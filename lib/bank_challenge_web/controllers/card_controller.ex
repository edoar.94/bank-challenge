defmodule BankChallengeWeb.CardController do
  use BankChallengeWeb, :controller

  alias BankChallenge.Core.Methods.CardMethods

  def index(conn, %{"account_id" => account_id}) do
    cards = CardMethods.cards(account_id)

    render(conn, "index.json", cards: cards)
  end
end
