defmodule BankChallenge.Core.Schemas.Card do
  use Ecto.Schema
  import Ecto.Changeset

  alias BankChallenge.Core.Schemas.{Account, Transaction}

  @derive {Jason.Encoder, except: [:__meta__, :account, :transactions, :inserted_at, :updated_at]}
  schema "cards" do
    field :type, :string, default: "debit"
    field :card_number, :string, default: "1111222233334444"
    field :balance, :integer, default: 0

    belongs_to :account, Account
    has_many :transactions, Transaction

    timestamps()
  end

  @required_fields ~w(account_id type card_number)a
  @optional_fields ~w(balance)a
  def changeset(%__MODULE__{} = data, params \\ %{}) do
    cast(data, params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_length(:card_number, is: 16)
    |> validate_format(:card_number, ~r{\A\d*\z})
  end
end
