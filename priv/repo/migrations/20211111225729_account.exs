defmodule BankChallenge.Repo.Migrations.Account do
  use Ecto.Migration

  def up do
    create table(:accounts) do
      add :name, :string
      add :lastname, :string
      add :phone, :string
      add :email, :string
      add :account_number, :string

      timestamps()
    end
  end

  def down do
    drop table(:accounts)
  end
end
