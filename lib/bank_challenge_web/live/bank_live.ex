defmodule BankChallengeWeb.BankLive do
  use Phoenix.LiveView
  alias BankChallengeWeb.AccountView
  alias BankChallenge.{APIRequest, Utils}

  def render(assigns),
    do: Phoenix.View.render(AccountView, "bank_live.html", assigns)

  def mount(_params, _assigns, socket) do
    if connected?(socket), do: BankChallengeWeb.Endpoint.subscribe("account:" <> "1")
    socket = assign(socket, %{
      account_id: "1",
      account: %{
        name: nil,
        cards: []
      },
      active: "active",
      content_active: "show active",
      tab: "1"
    })

    send(self(), :fetch_account)
    {:ok, socket}
  end

  def handle_info(:fetch_account, %{assigns: assigns} = socket) do
    with {:ok, %{"data" => account}} <- APIRequest.get("accounts/" <> assigns.account_id),
         {:ok, %{"data" => cards}} <- APIRequest.get("accounts/" <> assigns.account_id <> "/cards") do

      socket = assign(socket, %{
        account: Map.put(account, :cards, cards)
      })

      {:noreply, socket}
    end
  end

  def handle_info({:update_transactions, %{card: card}}, %{assigns: assigns} = socket) do
    account = Map.put(assigns.account, :cards, update_cards(card, assigns.account.cards))
    send_update BankChallengeWeb.TransactionComponent, id: card.id, tab: assigns.tab, account_id: assigns.account_id

    {:noreply, assign(socket, account: account)}
  end

  def handle_event("change_tab", %{"card_id" => card_id}, socket),
    do: {:noreply, assign(socket, %{tab: card_id})}

  defp update_cards(card, cards) do
    for l_card <- cards do
      if l_card["id"] == card.id do
        Utils.to_format_api_resp(card)
      else
        l_card
      end
    end
  end
end
