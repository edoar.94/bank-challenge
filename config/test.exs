use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :bank_challenge, BankChallenge.Repo,
  hostname: System.get_env("CORE_DB_HOST") || "postgres",
  database: System.get_env("CORE_DB_NAME") || "bank_challenge",
  username: System.get_env("CORE_DB_USERNAME") || "postgres",
  password: System.get_env("CORE_DB_PASSWORD") || "postgres",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :bank_challenge, BankChallengeWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
