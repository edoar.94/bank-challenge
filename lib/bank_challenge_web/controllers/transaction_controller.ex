defmodule BankChallengeWeb.TransactionController do
  use BankChallengeWeb, :controller

  alias BankChallenge.Core.Methods.TransactionMethods
  alias BankChallenge.Utils
  alias Phoenix.PubSub

  def index(conn, %{"card_id" => card_id}) do
    transactions = TransactionMethods.transactions(card_id)

    render(conn, "index.json", transactions: transactions)
  end

  def create(conn, %{"account_id" => account_id, "card_id" => card_id, "transaction" => transaction}) do
    Map.put(transaction, "card_id", card_id)
    |> TransactionMethods.transaction_multi()
    |> case do
      {:ok, %{card: card, transaction: transaction}} ->
        PubSub.broadcast(BankChallenge.PubSub, "account:#{account_id}", {:update_transactions, %{card: card, transaction: transaction}})
        render(conn, "transaction.json", transaction: transaction)
      {:error, _operation, error, _changeset} ->
        conn
        |> put_view(BankChallengeWeb.ErrorView)
        |> render("errors.json", errors: Utils.transform_errors(error))
    end
  end
end
